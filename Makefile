CFLAGS=-Wall -W -pipe -O2

rtwatch: rtwatch.o
	$(CC) $(CFLAGS) -o $@ $^

suid:
	chown root:root rtwatch
	chmod u+s rtwatch

clean:
	rm -f rtwatch rtwatch.o
